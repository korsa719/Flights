package ua.danit.application.resources;

import org.codehaus.jackson.map.ObjectMapper;
import ua.danit.application.dao.HotelDao;
import ua.danit.application.dao.TicketsDao;
import ua.danit.application.model.Hotel;
import ua.danit.application.model.Tickets;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import java.io.IOException;

public class TicketsResource {

    private TicketsDao ticketsDao;

    @Path("/tickets/")
    @GET
    public String getTickets() throws IOException {
        if (ticketsDao == null) {
            ticketsDao = new TicketsDao();
        }

        Iterable<Tickets> tickets = ticketsDao.getTopTickets(5);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(tickets);
    }

    @Path("/hotels/search")
    @GET
    public String getSearchTickets(@QueryParam("search") Long search) throws IOException {
        if (ticketsDao == null) {
            ticketsDao = new TicketsDao();
        }

        Iterable<Tickets> tickets = ticketsDao.searchTickets(search);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(tickets);
    }
}
